# Marginalia

## Demo

A demo is hosted at http://marginalia.press

## Local

To run locally, clone the repo then run the following:

`npm install`

`npm start`
