const path = require('path');

module.exports = {
    entry: path.join(__dirname, 'src/index.js'),
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'main.js'
    },
    module: {
        rules: [
          {
            test: /src\/\.js/,
            exclude: /node_modules/,
            use: [{
                loader: 'babel-loader'
            }]
          },
          {
            test: /\.css$/,
            use: [
              'style-loader',
              'css-loader'
            ]
          }
      ]
    },
    plugins: [],
    stats: {
        colors: true
    },
    devtool: 'source-map',
    devServer: {
      contentBase: './'
    }
};
