import ePub from 'epubjs';
import 'jszip';

class Manifest {
  constructor() {

  }

  async open(bookData) {
    this.book = await ePub(bookData, {replacements: true} );
    return this.process(this.book);
  }

  async process(book) {
    this.metadata(book.metadata);
    this.cover(book.cover);
    this.toc(book.toc);
    this.stylesheets = this.styles(book.resources);

    return this.contents(book.readingOrder || book.spine);
  }

  metadata(meta) {
    var $title = document.getElementById("title");
    var $author = document.getElementById("author");

    $title.textContent = meta.title;
    $author.textContent = meta.creator;
  }

  cover(cover) {
    var $cover = document.getElementById("cover");

    if (cover) {
      $cover.src = cover;
    } else {
      $cover.style.display = "none";
    }
  }

  toc(toc=[]) {
    let nav = document.getElementById("toc");
    let docfrag = document.createDocumentFragment();
    toc.forEach((chapter) => {
      this.tocItem(chapter, docfrag);
    });
    nav.appendChild(docfrag);
  }

  tocItem(chapter, parent) {
    let item = document.createElement("li");
    let link = document.createElement("a");
    link.id = "chap-" + chapter.id;
    link.textContent = chapter.title;
    link.href = "#" + encodeURIComponent(chapter.href);
    item.appendChild(link);
    parent.appendChild(item);

    link.onclick = function(){
      let nav = document.getElementById("toc");
      nav.classList.remove("open");
    };

    if (chapter.subitems && chapter.subitems.length) {
      var ul = document.createElement("ul");
      item.appendChild(ul);
      chapter.subitems.forEach(function(subchapter) {
        this.tocItem(subchapter, ul);
      });
    }

    // link.onclick = function(){
    //   var url = link.getAttribute("href");
    //   console.log(url)
    //   rendition.display(url);
    //   return false;
    // };
  }

  async contents(spine) {
    let text = "";
    let pattern = /<body[^>]*>((.|[\n\r])*)<\/body>/im;
    let main = /<main[^>]*>((.|[\n\r])*)<\/main>/im;
    let links = /<link[^>]*rel\s*=\s*["']\s*stylesheet\s*["'][^>]*>/im;

    for (let section of spine) {
      let href = section.href;
      let styles = "";
      let html = await fetch(href)
        .then((response) => {
          return response.text();
        }).then((t) => {
          let matches = pattern.exec(t);
          styles = links.exec(t);
          return matches && matches.length && matches[1];
        }).then((t) => {
          let matches = main.exec(t);
          return matches && matches.length ? matches[1] : t;
        });
      let id = encodeURIComponent(section.href);
      text += "<article id='"+id+"' class='marginalia-section'>";

      // if (styles) {
      //   for (let i = 0; i < styles.length; i++) {
      //     text += styles[i];
      //   }
      // }

      text += html + "</article>";
    }
    return text;
  }

  styles(resources) {
    return resources.filter((r) => {
      return r.type === "text/css";
    });
  }
}

export default Manifest;
