class Renderer {
  constructor(manifest) {

  }

  

  addStyles(polisher, resources) {
    let head = document.querySelector("head");
    let stylesArray = resources.filter((r) => {
      return r.type === "text/css";
    });
    let toAdd = ["styles/epub.css"];
    for (let style of stylesArray) {
      // let link = document.createElement("link");
      // link.rel = "stylesheet";
      // link.type = "text/css";
      // link.href = style.href;
      // head.appendChild(link);
      toAdd.push(style.href);
    }
    return polisher.add(...toAdd) //.apply(styles, toAdd);
  }

  clear() {
    let pages = document.querySelectorAll(".section");
    for (var i = 0; i < pages.length; i++) {
      pages[i].remove();
    }
  }
}

export default Renderer;
