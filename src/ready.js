export default new Promise(function(resolve, reject){
  if (document.readyState === "interactive" || document.readyState === "complete") {
    resolve(document.readyState);
    return;
  }

  document.onreadystatechange = function ($) {
    if (document.readyState === "interactive") {
      resolve(document.readyState);
    }
  }
});
