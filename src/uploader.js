class Uploader {
  constructor(onUpload) {
    this.onUpload = onUpload;

    this.form = document.getElementById("form");

    this.form.addEventListener("dragenter", this.dragenter.bind(this), false);
    this.form.addEventListener("dragover", this.dragover.bind(this), false);
    this.form.addEventListener("dragleave", this.dragleave.bind(this), false);
    this.form.addEventListener("drop", this.drop.bind(this), false);

    this.inputListener();
  }

  dragenter(e) {
    e.stopPropagation();
    e.preventDefault();
    this.form.classList.add("is-dragover");
  }

  dragover(e) {
    e.stopPropagation();
    e.preventDefault();
  }

  dragleave(e) {
    e.stopPropagation();
    e.preventDefault();
    this.form.classList.remove("is-dragover");
  }

  drop(e) {
    e.stopPropagation();
    e.preventDefault();

    var dt = e.dataTransfer;
    var files = dt.files;
    this.upload(files[0]);
  }

  inputListener() {
    let inputElement = document.getElementById("input");
    let choose = document.getElementById("choose");
    choose.addEventListener("click", () => {
      inputElement.click();
    }, false);


    inputElement.addEventListener('change', (e) => {
      var file = e.target.files[0];
      this.upload(file);
      this.form.style.display = "none";
    });
  }

  upload(file) {
    if (window.FileReader) {
      var reader = new FileReader();
      reader.onload = this.result.bind(this);
      reader.readAsArrayBuffer(file);
    }
  }

  result(e) {
    this.onUpload(e.target.result);
  }

  destroy() {

  }
}

export default Uploader;
