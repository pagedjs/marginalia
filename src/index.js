import ready from './ready';
import Reader from './reader';

ready.then(async function () {

  let params = URLSearchParams && new URLSearchParams(document.location.search.substring(1));
  let manifest = params && params.get("manifest") && decodeURIComponent(params.get("manifest"));

  let reader = new Reader(manifest);

});
