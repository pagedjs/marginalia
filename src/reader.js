import Uploader from './uploader';
import Manifest from './manifest';
import { Chunker, Polisher, initializeHandlers } from 'pagedjs';

class Reader {
  constructor(manifest) {
    this.uploader = new Uploader(this.uploaded.bind(this));

    if (manifest) {
      this.process(manifest);
    }
  }

  showUi() {
    let nav = document.getElementById("navigation");
    let opener = document.getElementById("opener");
    opener.style.visibility = "hidden";
    opener.addEventListener("click", this.open, false);

    let closer = document.getElementById("closer");
    closer.addEventListener("click", this.close, false);

    opener.style.visibility = "visible";
  }

  open() {
    let nav = document.getElementById("navigation");
    nav.classList.add("open");
  }

  close() {
    let nav = document.getElementById("navigation");
    nav.classList.remove("open");
  }

  hideInput() {
    let form = document.getElementById("form");
    form.style.display = "none";

    let logo = document.getElementById("logo");
    logo.style.display = "none";
  }

  async uploaded(bookData) {
    this.hideInput();
    this.showUi();

    this.manifest = new Manifest();
    let text = await this.manifest.open(bookData);

    this.render(text);

    let opener = document.getElementById("opener");
    opener.style.visibility = "visible";
  }

  async process(manifestUrl) {
    this.hideInput();
    this.showUi();

    let manifestJson = await fetch(manifestUrl)
      .then((response) => {
        return response.json();
      });

    // absolute links
    manifestJson.readingOrder.forEach((item) => {
      item.href = new URL(item.href, manifestUrl).toString();
    })

    this.manifest = new Manifest();
    let text = await this.manifest.process(manifestJson);

    this.render(text);

  }

  async render(text) {
    this.clear();

    this.chunker = new Chunker()
    this.polisher = new Polisher();

    initializeHandlers(this.chunker, this.polisher);

    let viewer = document.querySelector("#viewer");

    this.chunker.on("rendering", () => {
      this.resizer();
    });

    let page = 0;
    this.chunker.on("page", () => {
      page++;
      if (page === 2) {
        this.arrows(this.scale);
      }
    });



    window.addEventListener("resize", this.resizer.bind(this), false);

    await this.addStyles(this.manifest.stylesheets);

    return this.chunker.flow(text, viewer);
  }

  resizer() {
    let pages = document.querySelector(".pagedjs_pages");
    let scale = ((window.innerWidth ) / pages.offsetWidth) * .75;

    let style = document.createElement('style');
    style.setAttribute("media", "screen");
    document.head.appendChild(style);
    let sheet = style.sheet;

    if (scale < 1) {
      sheet.insertRule(`#viewer .pagedjs_pages { transform: scale(${scale}); margin-left: ${(window.innerWidth / 2) - ((pages.offsetWidth/ 2) )  * scale }px }`, sheet.cssRules.length);
    } else {
      pages.style.transform = "none";
      pages.style.marginLeft = "0";
    }

    this.scale = scale;

  }

  addStyles(stylesArray) {
    let toAdd = ["styles/epub.css"];
    for (let style of stylesArray) {
      toAdd.push(style.href);
    }
    return this.polisher.add(...toAdd);
  }

  clear() {
    let pages = document.querySelectorAll(".section");
    for (var i = 0; i < pages.length; i++) {
      pages[i].remove();
    }
  }

  arrows(scale) {
    let page = document.querySelector(".pagedjs_page");
    let height = (page.offsetHeight + 51) * scale;
    var next = document.getElementById("next");

    next.style.visibility = "visible";
    next.addEventListener("click", function(e){
      window.scrollBy(0, height);
      e.preventDefault();
    }, false);

    var prev = document.getElementById("prev");
    prev.style.visibility = "visible";

    prev.addEventListener("click", function(e){
      window.scrollBy(0, -height);
      e.preventDefault();
    }, false);

    var keyListener = function(e){

      // Left Key
      if ((e.keyCode || e.which) == 37) {
        window.scrollBy(0, -height);
      }

      // Right Key
      if ((e.keyCode || e.which) == 39) {
        window.scrollBy(0, height);
      }

    };

    document.addEventListener("keyup", keyListener, false);
  }

  destroy() {

  }
}

export default Reader;
